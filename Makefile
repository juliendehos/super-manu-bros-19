all:
	cabal build
	cp `find dist-newstyle -name all.js` public/
	# find public \( -name '*.js' \) -print0 | xargs -0 gzip -9 -k -f

run:
	python3 -m http.server -d public 3000

clean:
	rm -rf dist-newstyle public/all.js*


# Super Manu Bros 19

Jeu de type Super Mario Bros, réalisé dans le cadre du
[dixième week-end de programmation de jeux vidéo sur Developpez.com](https://www.developpez.net/forums/d2087507/applications/developpement-2d-3d-jeux/dixieme-week-end-programmation-jeux-video-developpez-com-11-septembre-13-septembre-2020-a/).

[Jouer en ligne !](https://juliendehos.gitlab.io/super-manu-bros-19)

![](doc/capture.mp4)


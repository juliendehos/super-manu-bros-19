module Collision (collideAllManuWall) where

import           Control.Lens ((&), (.~))
import qualified Data.Vector as V
import           Linear.Metric (qd)
import           Linear.V2

type Vec = V2 Double

collideAllManuWall :: Vec -> Vec -> Vec -> V.Vector Vec -> (Vec, Vec, Bool)
collideAllManuWall p0 p1 v pWalls = 
    let (V2 x0 y0) = p0
        (V2 x1 y1) = p1
        kx = (y1 - y0) / (x1 - x0)
        ky = (x1 - x0) / (y1 - y0)
        (p1a, va) = V.foldl' (collideFromTop p0 ky) (p1, v) pWalls
        (p1b, vb) = V.foldl' (collideFromLeft p0 kx) (p1a, va) pWalls
        (p1c, vc) = V.foldl' (collideFromRight p0 kx) (p1b, vb) pWalls
        (p1d, vd) = V.foldl' (collideFromBottom p0 ky) (p1c, vc) pWalls
        onGround = qd p1 p1a > 2
    in (p1d, vd, onGround)

collideFromTop :: Vec -> Double -> (Vec, Vec) -> Vec -> (Vec, Vec)
collideFromTop p0 ky (p1, v) pw =
    let (V2 x0 y0) = p0
        (V2 _x1 y1) = p1
        (V2 xw yw') = pw 
        yw = yw' + 50
        xi = x0 + (yw - y0) * ky
    in if y0 < yw || y1 > yw || xi < xw - 40 || xi > xw + 40
        then (p1, v)
        else (V2 xi (yw+1), v & _y .~ 0)

collideFromBottom :: Vec -> Double -> (Vec, Vec) -> Vec -> (Vec, Vec)
collideFromBottom p0 ky (p1, v) pw =
    let (V2 x0 y0) = p0
        (V2 _x1 y1) = p1
        (V2 xw yw') = pw 
        yw = yw' - 80
        xi = x0 + (yw - y0) * ky
    in if y0 > yw || y1 < yw || xi < xw - 50 || xi > xw + 50
        then (p1, v)
        else (V2 xi (yw-1), v & _y .~ 0)

collideFromLeft :: Vec -> Double -> (Vec, Vec) -> Vec -> (Vec, Vec)
collideFromLeft p0 kx (p1, v) pw =
    let (V2 x0 y0) = p0
        (V2 x1 y1) = p1
        (V2 xw' yw) = pw 
        xw = xw' - 50
        yi = y0 + (xw - x0) * kx
    in if x0 > xw || x1 < xw || yi < yw - 80 || yi > yw + 50
        then (p1, v)
        else (V2 (xw-1) y1, v & _x .~ 0)

collideFromRight :: Vec -> Double -> (Vec, Vec) -> Vec -> (Vec, Vec)
collideFromRight p0 kx (p1, v) pw =
    let (V2 x0 y0) = p0
        (V2 x1 y1) = p1
        (V2 xw' yw) = pw 
        xw = xw' + 50 
        yi = y0 + (xw - x0) * kx
    in if x0 < xw || x1 > xw || yi < yw - 80 || yi > yw + 50
        then (p1, v)
        else (V2 (xw+1) y1, v & _x .~ 0)


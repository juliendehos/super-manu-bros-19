module Action where

import qualified Data.Set as S

data Action
    = None
    | Reset { _time0 :: Double }
    | Display 
    | Step { _time :: Double }
    | Key { _key :: S.Set Int }


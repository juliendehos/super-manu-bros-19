module Game where

import           Control.Lens ((^.), (&), (.~))
import qualified Data.Vector as V
import           Linear.Metric (qd)
import           Linear.V2
import           Linear.Vector ((^+^), (*^))

import           Collision

gameWidth, gameHeight :: Int
gameWidth = 800
gameHeight = 600

gameWidthD, gameHeightD :: Double
gameWidthD = fromIntegral gameWidth
gameHeightD = fromIntegral gameHeight

gameMinX, gameMaxX, gameMinY, gameMaxY, gameSlideT :: Double
gameMinX = gameSlideT
gameMaxX = 10000
gameMinY = -gameHeightD
gameMaxY = gameHeightD - 100
gameSlideT = 200

gameGravity, gameJumpVel, gameMoveVel :: V2 Double
gameGravity = V2 0 (-600)
gameJumpVel = V2 0 400
gameMoveVel = V2 300 0

gameWalls, gameMasks, gameVirus :: V.Vector (V2 Double)
gameWalls = V.fromList
    $  [ V2 x 0     | x<-[0, 50.. 1800] ]
    ++ [ V2 600 50 ]
    ++ [ V2 1200 y  | y<-[50, 100] ]
    ++ [ V2 x 200   | x<-[1350, 1400] ]
    ++ [ V2 x 0     | x<-[2000, 2050.. 2200] ]

    -- 3
    ++ [ V2 x 0     | x<-[2300, 2350.. 2700] ]
    ++ [ V2 x 50    | x<-[2100, 2150, 2200] ]
    ++ [ V2 x 100   | x<-[2150, 2200] ]
    ++ [ V2 x 150   | x<-[2200] ]
    ++ [ V2 2350 y  | y<-[100] ]
    ++ [ V2 x 50    | x<-[2300, 2350.. 2450] ]

    -- 4 5
    ++ [ V2 x 0     | x<-[3000, 3050, 3100] ]
    ++ [ V2 x 0     | x<-[3300] ]
    ++ [ V2 x 0     | x<-[3650] ]
    ++ [ V2 x 0     | x<-[3800, 3850.. 4350] ]
    ++ [ V2 3900 y  | y<-[50, 100] ]
    ++ [ V2 x 200   | x<-[4200] ]
    ++ [ V2 x 300   | x<-[4400, 4450, 4500] ]
    ++ [ V2 x 250   | x<-[4700, 4750] ]

    -- 6 7
    ++ [ V2 x 0     | x<-[4550, 4600.. 5250] ]
    ++ [ V2 x 50    | x<-[5100, 5150, 5200] ]
    ++ [ V2 x 100   | x<-[5150, 5200] ]
    ++ [ V2 x 150   | x<-[5200] ]

    ++ [ V2 x 0     | x<-[5350, 5400, 5450] ]
    ++ [ V2 x 50    | x<-[5450, 5700] ]
    ++ [ V2 x 0     | x<-[5700, 5750.. 6650] ]

    -- 8 9
    ++ [ V2 6300 y  | y<-[50, 100] ]
    ++ [ V2 x 200   | x<-[6500] ]
    ++ [ V2 x 300   | x<-[6700] ]
    ++ [ V2 x 400   | x<-[6900] ]
    ++ [ V2 x 0     | x<-[6950, 7000.. 7200] ]

    -- 10
    ++ [ V2 x 0     | x<-[7500, 7550] ]
    ++ [ V2 7800 y  | y<-[50, 100] ]
    ++ [ V2 x 0     | x<-[7800, 7850.. 8350] ]

    -- final
    ++ [ V2 x 50    | x<-[8100, 8150, 8200, 8250, 8300, 8350] ]
    ++ [ V2 x 100   | x<-[8150, 8200, 8250, 8300, 8350] ]
    ++ [ V2 x 150   | x<-[8200, 8250, 8300, 8350] ]
    ++ [ V2 x 200   | x<-[8250, 8300, 8350] ]
    ++ [ V2 x 250   | x<-[8300, 8350] ]
    ++ [ V2 x 300   | x<-[8350] ]
    ++ [ V2 x 0     | x<-[8550, 8600, 8750, 8800, 8950, 9000, 9150, 9200] ]
    ++ [ V2 x 0     | x<-[9350, 9400.. 10400] ]

gameMasks = V.fromList
    [ V2 800 50, V2 1350 250, V2 2400 100, V2 4400 350, V2 4400 150
    , V2 5250 50, V2 5700 100, V2 7050 300, V2 7150 50, V2 7850 50 ]

gameVirus = V.fromList
    [ V2 900 50, V2 1650 50, V2 2600 50, V2 4500 350, V2 4850 50
    , V2 5350 50, V2 5900 50, V2 7150 300, V2 7050 50, V2 7950 50 ]

gameBoloss0, gameGoal :: V2 Double
gameBoloss0 = V2 8900 100
gameGoal = V2 9800 50

data Status = Welcome | Running | Won Int Int | Lost deriving (Eq)

data Game = Game
    { _inputLeft :: Bool
    , _inputRight :: Bool
    , _inputSpace :: Bool
    , _manuVel :: V2 Double
    , _manuPos :: V2 Double
    , _slideX :: Double
    , _masks :: V.Vector (V2 Double)
    , _virus :: V.Vector (V2 Double)
    , _wallFrustrum :: V.Vector (V2 Double)
    , _idleTime :: Double
    , _time :: Double
    , _bolossPos :: V2 Double
    , _onGround :: Bool
    , _status :: Status
    } deriving (Eq)

new :: Status -> Game
new = Game False False False manuVel manuPos 0 gameMasks gameVirus 
            V.empty 0 0 bolossPos False
    where manuVel = V2 0 0
          manuPos = V2 gameSlideT 300 
          -- GOD manuPos = V2 9500 300 
          bolossPos = gameBoloss0

boundXY :: V2 Double -> V2 Double
boundXY (V2 x y) = V2 xx y
    where xx = max gameMinX $ min gameMaxX x

reslideX :: Double -> Double -> Double
reslideX sx mx = 
    let sxMin = mx + 2*gameSlideT - gameWidthD
        sxMax = mx - gameSlideT
    in max sxMin $ min sxMax sx

step :: Double -> Game -> Game
step dt g@(Game inL inR inS manuVel manuPos slideX masks virus wallF idle 
            time bolossPos onGround status) 
    | status /= Running = g 
    | otherwise = 
        let 
            -- jump
            vel = if inS && onGround then gameJumpVel else manuVel
            -- left/right move
            dl = if inL then 0 else (-1)
            dr = if inR then 0 else 1
            dlr = dl + dr
            -- new position
            dxy00 = dt *^ (dlr *^ gameMoveVel ^+^ vel)
            xy00 = manuPos ^+^ dxy00
            -- floor
            (xy01, vel01) | xy00^._y > gameMaxY = (xy00 & _y .~ gameMaxY, vel & _y .~ 0)
                          | otherwise = (xy00, vel)
            -- wall
            (xy1, vel1, onGround') = collideAllManuWall manuPos xy01 vel01 wallF
            -- update status
            status1 | xy1^._y < (-150) = Lost 
                    | closerThan 20000 xy1 (bolossPos ^+^ V2 80 100) = Lost
                    | xy1^._x > gameMaxX - 50 = 
                        let nmasks = V.length gameMasks - V.length masks
                            nvirus = V.length gameVirus - V.length virus
                        in Won nmasks nvirus
                    | idle > 1 = Lost  -- GOD
                    | otherwise = Running
            manuPos' = boundXY xy1
        in g { _manuPos = manuPos'
             , _slideX = reslideX slideX (manuPos^._x)
             , _manuVel = vel1 ^+^ dt *^ gameGravity 
             , _status = status1
             , _masks = V.filter (notCloserThan 3000 xy1) masks
             , _virus = V.filter (notCloserThan 3000 xy1) virus
             , _wallFrustrum = frustrumV2s (xy1^._x) gameWalls
             , _idleTime = if qd manuPos manuPos' < 10 then idle + dt else 0
             , _time = time + dt
             , _bolossPos = gameBoloss0 ^+^ 
                    V2 (400 * sin (0.5*time)) (100 * sin (1*time))
             , _onGround = onGround'
             }

notCloserThan :: Double -> V2 Double -> V2 Double -> Bool
notCloserThan d2min xyA xyB = qd xyA xyB > d2min 

closerThan :: Double -> V2 Double -> V2 Double -> Bool
closerThan d2min xyA xyB = qd xyA xyB < d2min 

frustrumV2s :: Double -> V.Vector (V2 Double) -> V.Vector (V2 Double)
frustrumV2s mx = V.filter isInside
    where isInside (V2 x _) = mx - gameWidthD < x && x < mx + gameWidthD


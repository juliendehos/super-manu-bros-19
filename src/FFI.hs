module FFI where

import qualified JavaScript.Web.Canvas as JSC
import qualified Miso.String as MS

foreign import javascript unsafe "$r = new Image();"
    jsNewImage :: IO JSC.Image

foreign import javascript unsafe "$1.src = $2;"
    jsSetSrc :: JSC.Image -> MS.MisoString -> IO ()

foreign import javascript unsafe "$r = mycanvas.getContext('2d');"
    jsGetCtx :: IO JSC.Context

foreign import javascript unsafe "$4.drawImage($1, $2, $3);"
    jsDrawImage :: JSC.Image -> Double -> Double -> JSC.Context -> IO ()

foreign import javascript unsafe "console.log($1);"
    jsConsoleLog :: MS.MisoString -> IO ()

foreign import javascript unsafe "myaudio.play();"
    jsPlayAudio :: IO ()

foreign import javascript unsafe "myaudio.pause();"
    jsPauseAudio :: IO ()


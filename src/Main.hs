import           Miso

import qualified Action as A
import qualified Assets as S
import qualified Game as G
import qualified Model as M
import qualified Update as U
import qualified View as V

main :: IO ()
main = do
    assets <- S.mkAssets
    startApp App
        { initialAction = A.Display
        , update        = U.update assets
        , view          = V.view
        , model         = M.Model (G.new G.Welcome) 0.0
        , subs          = [ keyboardSub A.Key ]
        , events        = defaultEvents
        , mountPoint    = Nothing
        , logLevel      = Off
        }


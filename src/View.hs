{-# LANGUAGE OverloadedStrings #-}

module View where

import           Data.Map (singleton)
import qualified Data.Vector as V
import qualified Linear.V2 as L
import qualified JavaScript.Web.Canvas as JSC
import           Miso
import qualified Miso.String as MS

import qualified Action as A
import qualified Assets as S
import qualified FFI as F
import qualified Game as G
import qualified Model as M

view :: M.Model -> View A.Action
view _ = div_ []
    [ h1_ [] 
          [ a_ [ href_ "https://gitlab.com/juliendehos/super-manu-bros-19"]
               [ text "Super Manu Bros 19" ]
          ]
    , p_ [] [ audio_ [ id_ "myaudio", src_ "super-davie504.mp3", loop_ True ] [] ]
    , p_ [] [ gameDesc ]
    , p_ [] [ canvas_ [ id_ "mycanvas"
                      , width_ (MS.ms G.gameWidth)
                      , height_ (MS.ms G.gameHeight)
                      , style_  (singleton "border" "1px solid black")
                      ] []
            ]
    , p_ []
         [ a_ [ href_ "https://www.youtube.com/watch?v=-umlnQfRMAk"]
              [ text "Music by Davie504. Slap his YT channel now!" ]
         ]
    ]

drawInit :: IO JSC.Context
drawInit = do
    ctx <- F.jsGetCtx
    JSC.clearRect 0 0 G.gameWidthD G.gameHeightD ctx
    JSC.fillStyle 135 215 120 255 ctx
    JSC.fillRect 0 0 G.gameWidthD G.gameHeightD ctx
    return ctx

toCoord :: Double -> S.Asset -> L.V2 Double -> L.V2 Double
toCoord sx asset (L.V2 x y) = L.V2 (x-sx) (G.gameHeightD - h - y)
    where (L.V2 _ h) = S._size asset

drawItem :: Double -> S.Asset -> L.V2 Double -> JSC.Context -> IO ()
drawItem sx asset v ctx = do
    let (L.V2 x y) = toCoord sx asset v
    F.jsDrawImage (S._image asset) x y ctx

drawGame :: S.Assets -> G.Game -> IO ()
drawGame assets game = do
    ctx <- drawInit
    let sx = G._slideX game
        (L.V2 mx _) = G._manuPos game
    -- walls
    let wallItem = S._wall assets
    V.mapM_ (\v -> drawItem sx wallItem v ctx) (G._wallFrustrum game) 
    -- virus
    let virusItem = S._virus assets
    V.mapM_ (\v -> drawItem sx virusItem v ctx) (G.frustrumV2s mx $ G._virus game)
    -- masks
    let maskItem = S._mask assets
    V.mapM_ (\v -> drawItem sx maskItem v ctx) (G.frustrumV2s mx $ G._masks game)
    -- goal
    drawItem sx (S._goal assets) G.gameGoal ctx
    -- boloss
    drawItem sx (S._boloss assets) (G._bolossPos game) ctx
    -- manu
    drawItem sx (S._manu assets) (G._manuPos game) ctx

drawText :: MS.JSString -> IO ()
drawText txt = do
    ctx <- drawInit
    JSC.fillStyle 0 0 0 255 ctx
    JSC.textAlign JSC.Center ctx
    JSC.font "28px Arial" ctx
    JSC.fillText txt (0.5*G.gameWidthD) (0.5*G.gameHeightD) ctx
    JSC.stroke ctx

gameDesc :: View A.Action
gameDesc = 
    "Un Super Mario Bros, sauce COVID19. Manu doit capturer les 10 masques, \
    \en évitant les 10 coronavirus et le boloss-de-fin-à-l'arme-fatale... \
    \Et attention Manu : si tu t'arrêtes, tu meurs. Allez, appuie sur Enter \
    \pour mettre en marche !"


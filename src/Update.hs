{-# LANGUAGE OverloadedStrings #-}

module Update where

import qualified Data.Set as S
import           Miso
import qualified Miso.String as MS

import qualified Action as A
import qualified Assets as S
import qualified FFI as F
import qualified Game as G
import qualified Model as M
import qualified View as V

-------------------------------------------------------------------------------
-- utils
-------------------------------------------------------------------------------

myGetTime :: IO Double
myGetTime = (* 0.001) <$> now

usageDesc :: MS.MisoString
usageDesc = "Left/Right pour déplacer, Space pour sauter" 

-------------------------------------------------------------------------------
-- update
-------------------------------------------------------------------------------

update :: S.Assets -> A.Action -> M.Model -> Effect A.Action M.Model

update _ A.None m = noEff m

update _ (A.Reset t0) _ = m' <# pure A.Display
    where m' = M.Model (G.new G.Running) t0

update _ A.Display m = m <# case G._status (M._game m) of
    G.Welcome -> F.jsPauseAudio >> V.drawText usageDesc >> pure A.None
    G.Won nmasks nvirus -> do
        F.jsPauseAudio
        V.drawText $
            "Score: " <> MS.ms nmasks <> " mask(s) & " <> MS.ms nvirus <> " virus(es)"
        pure A.None
    G.Lost -> F.jsPauseAudio >> V.drawText "Game over !" >> pure A.None
    G.Running -> F.jsPlayAudio >> A.Step <$> myGetTime

update assets (A.Step t1) m = m' <# do
    V.drawGame assets (M._game m)
    pure A.Display 
    where t0 = M._time m
          dt = t1 - t0
          game = M._game m
          game' = G.step dt game
          m' = m { M._game = game', M._time = t1 }

update _ (A.Key ks) m = 
    if S.member 13 ks 
    then m <# do
        t <- myGetTime
        F.jsConsoleLog $ "new game at " <> MS.ms t
        pure $ A.Reset t
    else 
        let game = M._game m
            game' = game { G._inputLeft = S.member 39 ks
                         , G._inputRight = S.member 37 ks
                         , G._inputSpace = S.member 32 ks
                         }
        in noEff m { M._game = game' }


module Model where

import qualified Game as G

data Model = Model
    { _game :: G.Game
    , _time :: Double
    } deriving (Eq)


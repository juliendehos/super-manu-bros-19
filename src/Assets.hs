{-# LANGUAGE OverloadedStrings #-}

module Assets where

import qualified JavaScript.Web.Canvas as JSC
import qualified Linear.V2 as L
import qualified Miso.String as MS

import qualified FFI as F

data Asset = Asset
    { _image :: JSC.Image
    , _size :: L.V2 Double
    }

mkImage :: MS.MisoString -> L.V2 Double -> IO Asset
mkImage filename xy = do
    img <- F.jsNewImage
    F.jsSetSrc img filename
    return $ Asset img xy

data Assets = Assets
    { _boloss :: Asset
    , _goal :: Asset
    , _manu :: Asset
    , _mask :: Asset
    , _virus :: Asset
    , _wall :: Asset
    }

mkAssets :: IO Assets
mkAssets = Assets
    <$> mkImage "boloss.png" (L.V2 193 256)
    <*> mkImage "goal.png"   (L.V2 377 380)
    <*> mkImage "manu.png"   (L.V2 61  128)
    <*> mkImage "mask.png"   (L.V2 64  64)
    <*> mkImage "virus.png"  (L.V2 64  64)
    <*> mkImage "wall.gif"   (L.V2 50  50)

